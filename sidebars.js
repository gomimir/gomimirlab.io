module.exports = {
  docs: [
    'intro',
    'getting-started',
    'architecture/architecture',
    'metadata',
    'rules',
    'properties',
    {
      type: 'category',
      label: 'Processors',
      items: [
        'processors/overview',
        'processors/thumbnailer',
        'processors/thumbnailer-vips',
        'processors/text-extractor',
        'processors/exif-extractor',
        'processors/video',
        'processors/writing-your-own'
      ],
    },
    'unicode-normalization',
    {
      type: 'link',
      label: 'API reference',
      href: 'https://gomimir.gitlab.io/graphql/',
    },
  ],
};
