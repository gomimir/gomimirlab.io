# Mimir documentation

The documentation is built using [Docusaurus 2](https://v2.docusaurus.io/).

## Local Development

Before you begin, install the dependencies:

```console
yarn install
```

Then run:

```console
yarn start
```

This command starts a local development server and open up a browser window. Most changes are reflected live without having to restart the server.

