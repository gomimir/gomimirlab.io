const { default: axios } = require("axios");
const { loadSchema } = require("@graphql-tools/load");
const { UrlLoader } = require("@graphql-tools/url-loader");
const { introspectionFromSchema } = require("graphql");
const fs = require("fs");

async function getSchema() {
  const schemaUrl = await getLatestSchemaUrl();

  const schema = await loadSchema(schemaUrl, {
    loaders: [new UrlLoader()],
  });

  const intro = introspectionFromSchema(schema);
  fs.writeFileSync(
    "gqldocs/introspection.json",
    JSON.stringify({ data: intro }, null, 2)
  );
}

async function getLatestSchemaUrl() {
  const releases = await axios.get(
    "https://gitlab.com/api/v4/projects/24259511/releases"
  );
  if (releases.status !== 200) {
    throw new Error(`Unexpected status code ${releases.status}`);
  }

  const latestRelease = releases.data[0];
  if (!latestRelease) {
    throw new Error(`Unable to get latest release info`);
  }

  for (const link of latestRelease.assets.links) {
    if (link.name === "schema.graphql") {
      return link.direct_asset_url;
    }
  }

  throw new Error("Unable to find latest schema link");
}

getSchema();
