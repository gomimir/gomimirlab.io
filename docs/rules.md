---
title: Rules
---

Indexing rules exist for each index. While evaluating the rules, they are applied in order. Each rule can override any values set by the previous rules. Which means you should start from least specific to most.

You can specify rules when you starting your application for the first time or modify them any time later by editting index settings.

## Matching

Each rule can be applied based on the file path. The filter is specified using regular expression.

You can use named capture groups to use parts of the file path as a value (see [example with tags](#setting-tags))

While developping your rules we recommend testing it out with tools like [regexr](https://regexr.com/).

The regex expressions are implemented by standard Golang library. You can find details about the syntax here - [Google RE2 Syntax](https://github.com/google/re2/wiki/Syntax).

:::tip
Regular expressions are by default case sensitive. If you wish to match filenames case-insensitively prefix the expression with `(?i)` flag. Ie. like `(?i)\.(jpg|jpeg|cr2|arw|heic|mov|mp4|m4v)$` to match the file suffixes regardless of the case.
:::


## Actions

### Setting tags

You can automatically set asset tags based on file path. This can be useful for example if you are indexing photos which are organized in directories. Following example demonstrates how you can automatically assign tag with the name of the directory to all the photos it contains.

![Configuration screenshot](./images/screen-config-tag.png)

:::note
Note the `${album}` reference. It was defined as a capture group within the regular expression.
:::


### Grouping files into single asset

Lets imagine that you have scanned multi-page document but you haven't scanned it into single PDF, but you have multiple individual JPEG files instead - 1 file per page. Since all the files belong into a single document, you want to create just one asset from it.

You can use asset grouping key to solve the problem. Following snippet will instruct Mimir to group all files ending with `.page[0-9].jpg` suffix into single asset.

![Configuration screenshot](./images/screen-config-group.png)


### Processing

Processing action will queue tasks for processing whenever Mimir encounters files matching the pattern. See documentation of individual [Processors](/processors/) for more information about their options.

![Configuration screenshot](./processors/images/screen-config-tika.png)


### Ignoring certain files

To prevent certain file from indexing you can use ignore action. This can be especially helpful when combined with inverse matching.


![Configuration screenshot](./images/screen-config-ignore.png)

### Setting asset properties

You can set additional asset properties while indexing the file (additionally to those which are set by processors).

![Configuration screenshot](./images/screen-config-set.png)

:::tip
You can use special property `thumbnailPriority` to prioritize your JPEG files over RAWs.
:::