---
title: Introduction
slug: /
---

Mimir is a file indexing platform. It extracts valuable information from your files and allows you to search and organize them.

## Features

### Fast search

Mimir stores all the asset information in fast, search-optimized, document storage (Psst! It is Elasticsearch). Say goodbye to traversing directories structures. You can get what you need in matter of miliseconds!

### User properties support

Every use case is different. Mimir keeps you in charge of what kind of data you want to keep about your assets. You can define your own properties. Mimir will store them in type-safe manner and provides powerful search features including faceted search results! 

### Extendable

This project is built to support large variety cases. We want you to focus on what is important for your case. Mimir will solve the boring stuff for you. It is built to be extended by additional file processors. It even provides library for you to quickly bootstrap your own. Lets see what AI powered magic you can come up with!

### Structure agnostic

Have you spent countless of hours organizing your documents into awesome directory structure that suits your needs? Keep the structure! We are not going to force you to change what suits you. Even more, we can leverage it by offering you configurable grouping of your assets and tagging them based on your directories.

Not the tidiest man on earth? Mimir got you coverd too! Just upload your files. We won't tell. Mimir will sort the files just as well.
### Multiple repositories support

Do you want to have your files safely backed on cloud storage, but keep your thumbnails on your precious SSD for additional speed? You can do that :)

### Multi-tagging

You know what is more powerful than tagging? Multi-tagging! Mimir allows you to tag your assets with multiple kinds of tags! Do you want to tag them by file type? by place? by data sensitivity? You can do all of that :)



