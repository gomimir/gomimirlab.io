# Unicode normalization

While Mimir is trying to do its best to be encoding agnostic when it comes to filenames and their encoding it may happen that you can run into encoding issues nonetheless if you are running mixed environment (ie. MacOS + Windows / Linux). It is known that MacOS is normalizing all its filenames in NFD while other systems are using NFC. This can cause unexpected, hard to debug problems. Both of the forms produce visually same results but will fail equality checks on the byte level. This can cause some filename filters not to match for unexpected reasons to the end-user (ie. filename prefix when crawling for new files).

We recommend trying to stick with NFC if possible. 

## Converting to NFC

Easiest thing is to use [convmv](https://www.j3e.de/linux/convmv/man/)

On Ubuntu it can be installed by:

```bash
sudo apt-get install convmv
```

You can then use it for conversion like:

```bash
# Preview only
convmv -r -f utf8 -t utf8 --nfc .
# Apply changes (mind the --notest arg.)
convmv -r -f utf8 -t utf8 --nfc --notest .
```

## Links

- https://gist.github.com/JamesChevalier/8448512