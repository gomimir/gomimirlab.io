---
title: Getting started
---

:::caution
Ready to embark on a new journey with Mimir? The project is still under development. It is currently more suited for contributors. But you are welcome to give it a shot :)
:::


To get you started quickly, there is [docker-compose](https://gitlab.com/gomimir/docker-compose) repository prepared for you containing all that you need.

1. First clone it to your machine:

```bash
git clone https://gitlab.com/gomimir/docker-compose.git mimir-docker-compose
```

2. Go the directory which you just created

```bash
cd mimir-docker-compose
```

3. Start up the docker containers:

```bash
docker-compose up -d
```

:::note

Once everything has started, you should be able to reach the application in your web browser [http://localhost:4050](http://localhost:4050)

If you are adventurous you can also start exploring the GraphQL API [http://localhost:3050/playground](http://localhost:3050/playground).

:::
