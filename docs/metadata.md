---
title: Metadata
---

### Assets

*Asset* is a basic unit of Mimir metadata ecosystem. It represents single indexed item, consisting of one or more indexed files.

Assets are organized into collections called *Indices*. Each *Index* can be used to hold assets of some specific use-case. Ie. you can have 1 index for documents and 1 index for your photos. Or even more if it fits you case.

### Tags

*Assets* can be further categorized with help of tags. We have 2 level taxonomy for tags. Each tag has *Kind* and *Name*. Together they are crating unique pair within Mimir database. You can leverage this additional level of granularity to organize your *Assets*.

Here are few examples:

```
Kind: Security classification
Names: Public, Private, Secret
```
```
Kind: Album
Names: Summer vacation, Wedding, First Christmas
```

### Properties

Both *Assets* and their *Files* can have arbitrary number of properties assigned. They can be defined by the *User* or by the *Processors* used in your deployment. Because that they don't have to be known before hand and they are completely opaque to Mimir itself.

Mimir supports following types of properties:

<table>
  <thead>
    <tr>
      <td>Property&nbsp;type</td>
      <td>Description</td>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>
        <code>text</code>
      </th>
      <td>Contains text data. Used for the full-text search.</td>
    </tr>
    <tr>
      <th>
        <code>number</code>
      </th>
      <td>64-bit floating point number</td>
    </tr>
    <tr>
      <th>
        <code>fraction</code>
      </th>
      <td>Rational number in form of numerator and denominator, useful for storing otherwise unrepresentable numbers like 1/3</td>
    </tr>
    <tr>
      <th>
        <code>time</code>
      </th>
      <td>Date and time, including timezone</td>
    </tr>
    <tr>
      <th>
        <code>gps</code>
      </th>
      <td>GPS coordinates (latitude, longitude)</td>
    </tr>
  </tbody>
</table>


