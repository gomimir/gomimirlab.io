---
title: Architecture
slug: /architecture
---

## Application components

![Diagram](./assets/mimir-arch.svg)

**Mimir server** is the center of it all. It provides file access layer over Minio while offering *GraphQL API* to its clients. It indexes the file metadata and schedules processing jobs on newly discovered files. With processors it communicates with internal *GRPC API*.

**Mimir processors** (pictured in red) are where all the magic happens. They are responsible for extracting the file metadata. Also they can produce additional sidecar files (ie. thumbnails, analyzed profiles, ...). All processors are fully optional and you can include only those which fit your needs.

**Elasticsearch** is where we store all the metadata. Any information extracted by *Mimir processors* is stored there and indexed for fast searching.

**Redis** is used for distributing processing tasks and tracking their state.

**Minio** is where all the files are stored. You can optionally have multiple instances. Mimir is not bound strictly to Minio. You can use any S3-like service (ie. keep your originals in remote S3 and use local minio only for the *Sidecar files*).


## Stateless design

All application components are built to be without any local state for horizontal scaling out of the box.

## High-availability

Given the stateless design, it is easy to run application in multiple instances to achieve desired level of reliability.

Redis, Elasticsearch and Minio can be run in HA mode.

## Developer notes

All the dependencies (Elasticsearch, Redis, Minio) are used through abstract interface layer. This allows easy replacement if situation requires it. Abstract interface also offers easy service mocking for our test scenarios.
