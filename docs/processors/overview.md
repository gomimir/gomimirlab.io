---
title: Overview
slug: /processors/
---

Processors are what helps Mimir extract interesting information from your files and further enhance the experience. While it is perfectly fine to run Mimir without any processors, they bring additional value. You can run whatever number of them you like, you can even [bring your own](writing-your-own).


## Officially supported processors

We cooked those up for you:

- [Thumbnailer](thumbnailer) - Generates cover images to all your files. Photos and documents alike.
- [Thumbnailer (VIPS)](thumbnailer-vips) - Generates cover images to all your files. Optimized version for quick processing of JPEG and HEIC files.
- [Text extractor](text-extractor) - Enhances search experience by extracting contents of your documents.
- [EXIF extractor](exif-extractor) - Enhances your photo library by additional metadata information extracted from 
- [Video processor](video) - Extracts metadata and thumbnails from your videos.