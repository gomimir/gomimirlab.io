---
title: EXIF extractor
---

Repository: [GitLab](https://gitlab.com/gomimir/processor-exif)

EXIF extractor reads your photos and extract summary of EXIF tags if present.

### Docker compose snippet

```yaml title="docker-compose.yml"
services:
  # ...

  # EXIF extractor
  processor-exif:
    image: registry.gitlab.com/gomimir/processor-exif:latest
    environment:
      MIMIR_SERVER_HOST: change_to_your_mimir:3001
      MIMIR_REDIS_HOST: change_to_your_redis:6379
```

### Config example

![Configuration screenshot](./images/screen-config-exif.png)

