---
title: Writing your own
---

Writing your own processor is easy. Each processor is expected to be its own microservice and communicate with the server through GRPC API. You can have a look at the [Protobuf here](https://gitlab.com/gomimir/processor/-/blob/master/protobuf/mimir.proto).

You can write your processor in whatever language you like, but if you write in Go, you are in luck! There is a library already prepared for you to get you started.

## Writing own processor in Go

Initialize the project

```bash
go mod init processor-hello-world
```

Create the main source file

```go title="main.go"
package main

import (
	"fmt"
	"io/ioutil"

	mimir "gitlab.com/gomimir/processor"
	mimirapp "gitlab.com/gomimir/processor/pkg/app"
	mimirpb "gitlab.com/gomimir/processor/protobuf"
	mimirpbconv "gitlab.com/gomimir/processor/protobufconv"
)

func main() {
	// Create new application
	app := mimirapp.NewProcessorApp("processor-hello-world", "1.0.0")

	// Register file task handler for tasks named "doSomethingSpecial"
	app.RegisterFileProcessor("doSomethingSpecial", handler)

	// Listen to tasks in queue "myQueue" by default (can be overriden by CLI / ENV variables)
	app.SetDefaultProcessingQueue("myQueue")

	app.Execute()
}

func handler(req mimirapp.FileProcessorRequest) error {
	// Get file reader
	reader, err := req.Task.File().Reader()
	if err != nil {
		req.Log.Error().Err(err).Msg("Unable read file")
		return err
	}

	defer reader.Close()

	// Read file contents into memory
	bytes, err := ioutil.ReadAll(reader)
	if err != nil {
		req.Log.Error().Err(err).Msg("Error occurred while reading the file")
		return err
	}

	// TODO: DO SOMETHING WITH THE FILE

	// Update file properties
	props := mimir.PropertiesFromMap(map[string]interface{}{
		"myCustomProperty": fmt.Sprintf(
			"Greetings from my own processor! This file has %v bytes.",
			len(bytes),
		),
	})

	if req.IsTest {
		fmt.Println(props.String())
		return nil
	}

	convertedProps, err := mimirpbconv.PropertiesToProtoBuf(*props)
	if err != nil {
		req.Log.Error().Err(err).Msg("Failed to serialize asset properties")
		return err
	}

	_, err = req.Client.UpdateAssets(req.Command.Context(), &mimirpb.UpdateAssetsRequest{
		Selector: &mimirpb.UpdateAssetsRequest_Ref{
			Ref: &mimirpb.AssetRef{
				Index:   req.Task.File().AssetRef().IndexName(),
				AssetId: req.Task.File().AssetRef().AssetID(),
			},
		},
		UpdateFiles: []*mimirpb.UpdateAssetFileRequest{
			{
				Selector: &mimirpb.UpdateAssetFileRequest_Ref{
					Ref: &mimirpb.FileRef{
						Repository: req.Task.File().FileRef().Repository,
						Filename:   req.Task.File().FileRef().Filename,
					},
				},
				SetProperties: convertedProps,
			},
		},
	})

	return err
}
```

Download all the dependencies

```bash
go mod tidy
```

Make the test run

```bash
go run main.go test doSomethingSpecial some_file.jpg
```

Once you are satisfied with the results, you can register your processor in Mimir by adding processing rule. Edit your index settings and add following rule:

![Configuration screenshot](./images/screen-config-helloworld.png)

You can pass whatever JSON object to the config. It will then be available to your processor in `req.Task().Parameters()`.

Once registered you can finally put your brand new processor to the test with Mimir (assuming you have it already running on your localhost):

```bash
go run main.go process
```

:::note

You might want to adjust the URLs to fit your setup. We recommend doing that by env. variables `MIMIR_SERVER_HOST` and `MIMIR_REDIS_HOST`.

:::

With it running, try uploading some JPEG files (or trigger crawling of the existing files).

## Further resources

To see real-life example, you can have a look for example on official [EXIF extractor](https://gitlab.com/gomimir/processor-exif).