---
title: Text extractor
---

Repository: [GitLab](https://gitlab.com/gomimir/processor-tika)

Text extractor reads your files and extracts their content in form of plain text which is then indexed in Mimir search engine. The extraction is done using battle tested Apache Tika and supports all common [document formats](https://tika.apache.org/1.4/formats.html) including PDFs and MS Office documents. In case of image data, the extractor first runs OCR on the images to help to get text content even from your scans.



### Docker compose snippet

```yaml title="docker-compose.yml"
services:
  # ...

  # Text extractor
  processor-tika:
    image: registry.gitlab.com/gomimir/processor-tika:latest
    environment:
      MIMIR_SERVER_HOST: change_to_your_mimir:3001
      MIMIR_REDIS_HOST: change_to_your_redis:6379
      MIMIR_TIKA_URL: http://tika:9998

  # Apache Tika
  tika:
    image: registry.gitlab.com/gomimir/processor-tika/tika:latest

```

### Config example

![Configuration screenshot](./images/screen-config-tika.png)

#### OCR Language

Optionally, you can pass language for improved OCR detection.

Available languages are: `eng`, `ita`, `fra`, `spa`, `deu`, `ces`

```js title="OCR configuration"
{
  "ocrLanguage": "ces"
}
```
