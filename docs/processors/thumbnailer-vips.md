---
title: Thumbnailer (VIPS)
---

Repository: [GitLab](https://gitlab.com/gomimir/processor-vips)

Thumbnailer processor is reponsible for creating cover images for your files. This is an optimized version build upon [libvips](https://github.com/libvips/libvips). It offers better performance but does not support all the file formats Imagick does. If you are generating thumbnails just for the JPEG and HEIC images then this is a good choice. If you are looking for generating thumbnails for RAW files or stuff like PDFs, choose [Imagick version](thumbnailer) instead.


### Docker compose snippet

```yaml title="docker-compose.yml"
services:
  # ...

  # Thumbnailer (VIPS)
  processor-vips:
    image: registry.gitlab.com/gomimir/processor-vips:latest
    environment:
      MIMIR_SERVER_HOST: change_to_your_mimir:3001
      MIMIR_REDIS_HOST: change_to_your_redis:6379
```

### Config example

![Configuration screenshot](./images/screen-config-vips.png)
