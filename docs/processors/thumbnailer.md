---
title: Thumbnailer
---

Repository: [GitLab](https://gitlab.com/gomimir/processor-imagick)

Thumbnailer processor is reponsible for creating cover images for your files. It uses ImageMagick which gives it support for large variety of [file formats](https://imagemagick.org/script/formats.php). 

### Docker compose snippet

```yaml title="docker-compose.yml"
services:
  # ...

  # Thumbnailer
  processor-imagick:
    image: registry.gitlab.com/gomimir/processor-imagick:latest
    environment:
      MIMIR_SERVER_HOST: change_to_your_mimir:3001
      MIMIR_REDIS_HOST: change_to_your_redis:6379
```

### Config example

![Configuration screenshot](./images/screen-config-imagick.png)
