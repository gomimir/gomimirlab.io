---
title: Video processor
---

Repository: [GitLab](https://gitlab.com/gomimir/processor-ffmpeg)

Video processor takes care of extracting metadata from your videos and generating preview thumbnails. It uses [FFmpeg](https://ffmpeg.org/) to do all the heavy lifting under the hood.

### Docker compose snippet

```yaml title="docker-compose.yml"
services:
  # ...

  # Video processor
  processor-ffmpeg:
    image: registry.gitlab.com/gomimir/processor-ffmpeg:latest
    environment:
      MIMIR_SERVER_HOST: change_to_your_mimir:3001
      MIMIR_REDIS_HOST: change_to_your_redis:6379
```

### Config example

![Configuration screenshot](./images/screen-config-ffmpeg.png)
