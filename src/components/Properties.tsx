import * as React from "react";

export const Properties: React.FC = ({ children }) => (
  <table>
    <thead>
      <tr>
        <td>Property&nbsp;name</td>
        <td>Property&nbsp;type</td>
        <td>Description</td>
      </tr>
    </thead>
    <tbody>{children}</tbody>
  </table>
);

const PROCESSOR_MAP = {
  video: () => <a href="processors/video">Video processor</a>,
  exif: () => <a href="processors/exif-extractor">EXIF extractor</a>,
  text: () => <a href="processors/text-extractor">Text extractor</a>,
};

export const PropertyDefinition: React.FC<{
  name: string;
  type: "string" | "number";
  populatedBy: Array<keyof typeof PROCESSOR_MAP>;
}> = ({ name, type, populatedBy, children }) => (
  <tr>
    <td>
      <code>{name}</code>
    </td>
    <td align="center">
      <code>{type}</code>
    </td>
    <td>
      {children}
      {populatedBy && (
        <div>
          Populated by:
          {populatedBy.map((p, i) => (
            <>
              {i === 0 ? " " : ", "}
              {PROCESSOR_MAP[p] ? React.createElement(PROCESSOR_MAP[p]) : p}
            </>
          ))}
        </div>
      )}
    </td>
  </tr>
);
